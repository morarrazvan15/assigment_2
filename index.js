const FIRST_NAME = "RAZVAN-GABRIEL";
const LAST_NAME = "MORAR";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
function initCaching() {
var g={};
var counter=0;

var cache={};
cache.pageAccessCounter=function(S='home')
{
var f=S.toLocaleLowerCase();
if(g[f]===undefined||g[f]===null)
{
    g[f]=1;
}
else
{
g[f]++;
}

};
cache.getCache=function()
{
    return g;
};
return cache;


}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

